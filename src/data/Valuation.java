package data;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Pair;
import util.Sets;

import static java.util.stream.Collectors.toSet;
import static util.Objects.assertNonNull;


@NonNullByDefault
public class Valuation {
  private Map<DecoratedAttribute, Value> map;

  public Valuation() {
    this.map = new HashMap<>();
  }

  public Valuation(Map<DecoratedAttribute, Value> map) {
    this.map = new HashMap<>(map);
  }

  public @Nullable Value getValue(DecoratedAttribute decoratedAttribute) {
    return this.map.get(decoratedAttribute);
  }

  public Valuation with(DecoratedAttribute decoratedAttribute, Value value) {
    var v = new Valuation(this.map);
    v.map.put(decoratedAttribute, value);
    return v;
  }

  public Valuation getPre() {
    var pre = new Valuation();
    for (DecoratedAttribute decoratedAttribute : this.map.keySet()) {
      var unprime = decoratedAttribute.unprime();
      var unprimeValue = this.map.get(unprime);
      if (unprimeValue != null)
        pre.map.put(unprime, unprimeValue);
    }
    return pre;
  }

  public Valuation getPost() {
    var post = new Valuation();
    for (DecoratedAttribute decoratedAttribute : this.map.keySet()) {
      var unprime = decoratedAttribute.unprime();
      var prime = decoratedAttribute.prime();
      var primeValue = this.map.get(prime);
      if (primeValue != null)
        post.map.put(unprime, primeValue);
    }
    return post;
  }

  public Pair<Valuation> getPrePost() {
    var pre = new Valuation();
    var post = new Valuation();
    for (DecoratedAttribute decoratedAttribute : this.map.keySet()) {
      var unprime = decoratedAttribute.unprime();
      var prime = decoratedAttribute.prime();
      var unprimeValue = this.map.get(unprime);
      if (unprimeValue != null)
        pre.map.put(unprime, unprimeValue);
      var primeValue = this.map.get(prime);
      if (primeValue != null)
        post.map.put(unprime, primeValue);
    }
    return new Pair<>(pre, post);
  }

  public Valuation reduct(Map<Attribute, Attribute> attributesMap) {
    var reduct = new Valuation();
    for (Attribute attribute : attributesMap.keySet()) {
      var unprime = attribute;
      var prime = attribute.prime();
      var mappedUnprime = attributesMap.get(attribute);
      if (mappedUnprime == null)
        continue;
      var mappedPrime = mappedUnprime.prime();
      var unprimeValue = this.map.get(mappedUnprime);
      if (unprimeValue != null)
        reduct.map.put(unprime, unprimeValue);
      var primeValue = this.map.get(mappedPrime);
      if (primeValue != null)
        reduct.map.put(prime, primeValue);
    }
    return reduct;
  }

  public Valuation restrict(Set<Attribute> attributes) {
    var reduct = new Valuation();
    for (Attribute attribute : attributes) {
      var unprime = attribute;
      var prime = attribute.prime();
      var unprimeValue = this.map.get(unprime);
      if (unprimeValue != null)
        reduct.map.put(unprime, unprimeValue);
      var primeValue = this.map.get(prime);
      if (primeValue != null)
        reduct.map.put(prime, primeValue);
    }
    return reduct;
  }

  public static Valuation prePost(Valuation pre, Valuation post) {
    var v = new Valuation(pre.map);
    for (DecoratedAttribute attribute : post.map.keySet()) {
      var value = post.map.get(attribute);
      if (value == null)
        continue;
      v.map.put(attribute.prime(), value);
    }
    return v;
  }

  public static Valuation prePost(Pair<Valuation> prePost) {
    return Valuation.prePost(prePost.getFirst(), prePost.getSecond());
  }

  private static Map<Set<DecoratedAttribute>, Set<Valuation>> valuationsMap = new HashMap<>();

  private static Set<Valuation> allUncached(Set<DecoratedAttribute> decoratedAttributes) {
    if (decoratedAttributes.isEmpty())
      return Sets.empty();

    var decoratedAttribute = decoratedAttributes.iterator().next();

    if (decoratedAttributes.size() == 1)
      return decoratedAttribute.getRange().stream().map(value -> new Valuation().with(decoratedAttribute, value)).collect(toSet());

    var remainingUncached = allUncached(Sets.difference(decoratedAttributes, Sets.singleton(decoratedAttribute)));
    return assertNonNull(remainingUncached.stream().
             map(valuation -> decoratedAttribute.getRange().stream().
               map(value -> valuation.with(decoratedAttribute, value)).collect(toSet())).reduce(Sets.empty(), Sets::union));
  }

  public static Set<Valuation> all(Set<DecoratedAttribute> decoratedAttributes) {
    var cachedValuations = valuationsMap.get(decoratedAttributes);
    if (cachedValuations != null)
      return cachedValuations;

    cachedValuations = allUncached(decoratedAttributes);
    valuationsMap.put(decoratedAttributes, cachedValuations);
    return cachedValuations;
  }

  public boolean satisfies(Predicate predicate) {
    return predicate.holdsAt(this);
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.map);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      Valuation other = (Valuation)object;
      return Objects.equals(this.map, other.map);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return Formatter.map(map);
  }
}
