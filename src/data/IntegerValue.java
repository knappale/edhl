package data;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class IntegerValue extends Value {
  private int v;

  public IntegerValue(int v) {
    this.v = v;
  }

  public boolean lt(IntegerValue other) {
    return this.v < other.v;
  }

  public boolean leq(IntegerValue other) {
    return this.v <= other.v;
  }

  @Override
  public boolean eq(Value value) {
    return this.equals(value);
  }

  public boolean geq(IntegerValue other) {
    return this.v >= other.v;
  }

  public boolean gt(IntegerValue other) {
    return this.v > other.v;
  }
  
  public IntegerValue plus(IntegerValue other) {
    return new IntegerValue(this.v + other.v);
  }
  
  public IntegerValue minus(IntegerValue other) {
    return new IntegerValue(this.v - other.v);
  }

  public IntegerValue div(IntegerValue other) {
    return new IntegerValue(this.v / other.v);
  }

  public IntegerValue mod(IntegerValue other) {
    return new IntegerValue(this.v % other.v);
  }

  @Override
  public int hashCode() {
    return this.v;
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      IntegerValue other = (IntegerValue)object;
      return this.v == other.v;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.v);
    return builder.toString();
  }
}
