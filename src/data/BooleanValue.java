package data;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class BooleanValue extends Value {
  private boolean b;

  public BooleanValue(boolean b) {
    this.b = b;
  }

  @Override
  public boolean eq(Value value) {
    return this.equals(value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.b);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      BooleanValue other = (BooleanValue)object;
      return this.b == other.b;
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    var builder = new StringBuilder();
    builder.append(this.b);
    return builder.toString();
  }
}
