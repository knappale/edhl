package data;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;


@NonNullByDefault
public abstract class Value extends Expression {
  public abstract boolean eq(Value other);

  public boolean neq(Value other) {
    return !this.eq(other);
  }

  public Set<Attribute> getAttributes() {
    return Sets.empty();
  }

  public Set<DecoratedAttribute> getDecoratedAttributes() {
    return Sets.empty();
  }

  public @Nullable Value getValue(Valuation valuation) {
    return this;
  }

  @Override
  public int getPrecedence() {
    return 0;
  }
}
