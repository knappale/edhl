package data;

import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Sets;


@NonNullByDefault
public abstract class Expression {
  public static class Add extends Expression {
    private Expression left, right;

    public Add(Expression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    public Add(int left, Expression right) {
      this(new IntegerValue(left), right);
    }

    public Add(Expression left, int right) {
      this(left, new IntegerValue(right));
    }

    @Override
    public int getPrecedence() {
      return 4;
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public @Nullable IntegerValue getValue(Valuation valuation) {
      var leftValue = this.left.getValue(valuation);
      if (leftValue == null || !(leftValue instanceof IntegerValue))
        return null;
      var rightValue = this.right.getValue(valuation);
      if (rightValue == null || !(rightValue instanceof IntegerValue))
        return null;
      return ((IntegerValue)leftValue).plus((IntegerValue)rightValue);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Add other = (Add)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, p -> p.hasPrecedence(this)));
      builder.append(" + ");
      builder.append(Formatter.parenthesised(this.right, p -> p.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Mod extends Expression {
    private Expression left, right;

    public Mod(Expression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    public Mod(int left, Expression right) {
      this(new IntegerValue(left), right);
    }

    public Mod(Expression left, int right) {
      this(left, new IntegerValue(right));
    }

    @Override
    public int getPrecedence() {
      return 3;
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public @Nullable IntegerValue getValue(Valuation valuation) {
      var leftValue = this.left.getValue(valuation);
      if (leftValue == null || !(leftValue instanceof IntegerValue))
        return null;
      var rightValue = this.right.getValue(valuation);
      if (rightValue == null || !(rightValue instanceof IntegerValue))
        return null;
      return ((IntegerValue)leftValue).mod((IntegerValue)rightValue);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Add other = (Add)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, p -> p.hasPrecedence(this)));
      builder.append(" % ");
      builder.append(Formatter.parenthesised(this.right, p -> p.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public abstract int getPrecedence();

  public boolean hasPrecedence(Expression other) {
    return this.getPrecedence() <= other.getPrecedence();
  }

  public abstract Set<Attribute> getAttributes();
  public abstract @Nullable Value getValue(Valuation valuation);
}
