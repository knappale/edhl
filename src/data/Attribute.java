package data;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import static java.util.stream.Collectors.toSet;

import util.Sets;


@NonNullByDefault
public class Attribute extends DecoratedAttribute {
  private String name;
  private Set<Value> range;

  public Attribute(String name, Set<Value> range) {
    this.name = name;
    this.range = range;
  }

  public String getName() {
    return this.name;
  }

  public Set<Value> getRange() {
    return this.range;
  }

  public DecoratedAttribute prime() {
    return new Primed(this);
  }

  public Attribute unprime() {
    return this;
  }

  public static Set<DecoratedAttribute> getDecoratedAttributes(Set<Attribute> attributes) {
    return Sets.union(attributes.stream().map(Attribute::unprime).collect(toSet()),
                      attributes.stream().map(Attribute::prime).collect(toSet()));
  }

  @Override
  public Set<Attribute> getAttributes() {
    return Sets.singleton(this);
  }

  @Override
  public @Nullable Value getValue(Valuation valuation) {
    return valuation.getValue(this);
  }

  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      Attribute other = (Attribute)object;
      return this.name.equals(other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.name;
  }
}
