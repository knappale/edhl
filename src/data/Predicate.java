package data;

import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Sets;

import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public abstract class Predicate {
  private static Predicate FALSE = new False();
  private static Predicate TRUE = new True();

  public static class Lt extends Predicate {
    private Expression left, right;

    public Lt(Expression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    public Lt(int left, Expression right) {
      this(new IntegerValue(left), right);
    }

    public Lt(Expression left, int right) {
      this(left, new IntegerValue(right));
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      var leftValue = this.left.getValue(valuation);
      if (leftValue == null || !(leftValue instanceof IntegerValue))
        return false;
      var rightValue = this.right.getValue(valuation);
      if (rightValue == null || !(rightValue instanceof IntegerValue))
        return false;
      return ((IntegerValue)leftValue).lt((IntegerValue)rightValue);
    }

    @Override
    public int getPrecedence() {
      return 6;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Lt other = (Lt)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.left);
      builder.append(" < ");
      builder.append(this.right);
      return builder.toString();
    }
  }

  public static class Leq extends Predicate {
    private Expression left, right;

    public Leq(Expression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    public Leq(int left, Expression right) {
      this(new IntegerValue(left), right);
    }

    public Leq(Expression left, int right) {
      this(left, new IntegerValue(right));
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      var leftValue = this.left.getValue(valuation);
      if (leftValue == null || !(leftValue instanceof IntegerValue))
        return false;
      var rightValue = this.right.getValue(valuation);
      if (rightValue == null || !(rightValue instanceof IntegerValue))
        return false;
      return ((IntegerValue)leftValue).leq((IntegerValue)rightValue);
    }

    @Override
    public int getPrecedence() {
      return 6;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Leq other = (Leq)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.left);
      builder.append(" <= ");
      builder.append(this.right);
      return builder.toString();
    }
  }

  public static class Eq extends Predicate {
    private Expression left, right;

    public Eq(Expression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    public Eq(boolean left, Expression right) {
      this(new BooleanValue(left), right);
    }

    public Eq(Expression left, boolean right) {
      this(left, new BooleanValue(right));
    }

    public Eq(int left, Expression right) {
      this(new IntegerValue(left), right);
    }

    public Eq(Expression left, int right) {
      this(left, new IntegerValue(right));
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      var leftValue = this.left.getValue(valuation);
      if (leftValue == null)
        return false;
      var rightValue = this.right.getValue(valuation);
      if (rightValue == null)
        return false;
      return leftValue.eq(rightValue);
    }

    @Override
    public int getPrecedence() {
      return 7;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Eq other = (Eq)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.left);
      builder.append(" == ");
      builder.append(this.right);
      return builder.toString();
    }
  }

  public static class Neq extends Predicate {
    private Expression left, right;

    public Neq(Expression left, Expression right) {
      this.left = left;
      this.right = right;
    }

    public Neq(boolean left, Expression right) {
      this(new BooleanValue(left), right);
    }

    public Neq(Expression left, boolean right) {
      this(left, new BooleanValue(right));
    }

    public Neq(int left, Expression right) {
      this(new IntegerValue(left), right);
    }

    public Neq(Expression left, int right) {
      this(left, new IntegerValue(right));
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return !new Predicate.Eq(this.left, this.right).holdsAt(valuation);
    }

    @Override
    public int getPrecedence() {
      return 7;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Neq other = (Neq)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.left);
      builder.append(" != ");
      builder.append(this.right);
      return builder.toString();
    }
  }

  public static class Id extends Predicate {
    private Set<Attribute> attributes;
    private Predicate derived;

    public Id(Set<Attribute> attributes) {
      this.attributes = attributes;
      this.derived = attributes.stream().map(Id::id).reduce(new True(), And::new);
    }

    private static Predicate id(Attribute attribute) {
      return new Predicate.Eq(attribute, attribute.prime());
    }

    @Override
    public Set<Attribute> getAttributes() {
      return this.attributes;
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return this.derived.holdsAt(valuation);
    }

    @Override
    public int getPrecedence() {
      return this.derived.getPrecedence();
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.attributes);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Id other = (Id)object;
        return Objects.equals(this.attributes, other.attributes);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append("id(");
      builder.append(Formatter.set(this.attributes));
      builder.append(")");
      return builder.toString();
    }
  }

  private static class False extends Predicate {
    public False() {
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.empty();
    }

    @Override
    public Set<Valuation> getValuations(Set<DecoratedAttribute> attributes) {
      return Sets.empty();
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return false;
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return (this.getClass() == object.getClass());
    }

    @Override
    public String toString() {
      return "false";
    }
  }
    
  private static class True extends Predicate {
    public True() {
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.empty();
    }

    @Override
    public Set<Valuation> getValuations(Set<DecoratedAttribute> attributes) {
      return Valuation.all(attributes);
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return true;
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return (this.getClass() == object.getClass());
    }

    @Override
    public String toString() {
      return "true";
    }
  }
    
  private static class Not extends Predicate {
    private Predicate pred;

    public Not(Predicate pred) {
      this.pred = pred;
    }

    @Override
    public Set<Attribute> getAttributes() {
      return this.pred.getAttributes();
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return !this.pred.holdsAt(valuation);
    }

    @Override
    public int getPrecedence() {
      return 2;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.pred);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Not other = (Not)object;
        return Objects.equals(this.pred, other.pred);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append("!");
      builder.append(Formatter.parenthesised(this.pred, p -> p.hasPrecedence(this)));
      return builder.toString();
    }
  }

  private static class And extends Predicate {
    private Predicate left, right;

    public And(Predicate left, Predicate right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return this.left.holdsAt(valuation) && this.right.holdsAt(valuation);
    }

    @Override
    public int getPrecedence() {
      return 11;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        And other = (And)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, p -> p.hasPrecedence(this)));
      builder.append(" && ");
      builder.append(Formatter.parenthesised(this.right, p -> p.hasPrecedence(this)));
      return builder.toString();
    }
  }

  private static class Or extends Predicate {
    private Predicate left, right;

    public Or(Predicate left, Predicate right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.union(this.left.getAttributes(), this.right.getAttributes());
    }

    @Override
    public boolean holdsAt(Valuation valuation) {
      return this.left.holdsAt(valuation) || this.right.holdsAt(valuation);
    }

    @Override
    public int getPrecedence() {
      return 12;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Or other = (Or)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, p -> p.hasPrecedence(this)));
      builder.append(" || ");
      builder.append(Formatter.parenthesised(this.right, p -> p.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public abstract int getPrecedence();

  public boolean hasPrecedence(Predicate other) {
    return this.getPrecedence() <= other.getPrecedence();
  }

  public static Predicate False() {
    return FALSE;
  }

  public static Predicate True() {
    return TRUE;
  }

  public static Predicate not(Predicate predicate) {
    if (predicate.equals(FALSE))
      return TRUE;
    if (predicate.equals(TRUE))
      return FALSE;
    return new Not(predicate);
  }

  public static Predicate and(Predicate left, Predicate right) {
    if (left.equals(FALSE) || right.equals(FALSE))
      return FALSE;
    if (left.equals(TRUE))
      return right; 
    if (right.equals(TRUE))
      return left;
    if (left.equals(right))
      return left;
    return new And(left, right);
  }

  public static Predicate and(Set<Predicate> predicates) {
    return predicates.stream().reduce(TRUE, Predicate::and);
  }

  public static Predicate or(Predicate left, Predicate right) {
    if (left.equals(TRUE) || right.equals(TRUE))
      return TRUE;
    if (left.equals(FALSE))
      return right; 
    if (right.equals(FALSE))
      return left;
    if (left.equals(right))
      return left;
    return new Or(left, right);
  }

  public static Predicate or(Set<Predicate> predicates) {
    return predicates.stream().reduce(FALSE, Predicate::or);
  }

  public abstract Set<Attribute> getAttributes();

  public abstract boolean holdsAt(Valuation valuation);

  public Set<Valuation> getValuations(Set<DecoratedAttribute> decoratedAttributes) {
    return Valuation.all(decoratedAttributes).stream().filter(valuation -> this.holdsAt(valuation)).collect(toSet());
  }

  public boolean isSatisfiable(Set<DecoratedAttribute> decoratedAttributes) {
    return !this.getValuations(decoratedAttributes).isEmpty();
  }

  public abstract String toString();
}
