package data;

import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Sets;


@NonNullByDefault
public abstract class DecoratedAttribute extends Expression {
  public static class Primed extends DecoratedAttribute {
    private Attribute attribute;

    Primed(Attribute attribute) {
      this.attribute = attribute;
    }

    @Override
    public DecoratedAttribute prime() {
      return this;
    }

    @Override
    public DecoratedAttribute unprime() {
      return this.attribute;
    }

    @Override
    public Set<Value> getRange() {
      return this.attribute.getRange();
    }

    @Override
    public Set<Attribute> getAttributes() {
      return Sets.singleton(this.attribute);
    }

    @Override
    public @Nullable Value getValue(Valuation valuation) {
      return valuation.getValue(this);
    }

    @Override
    public int hashCode() {
      return this.attribute.hashCode();
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Primed other = (Primed)object;
        return Objects.equals(this.attribute, other.attribute);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      var builder = new StringBuilder();
      builder.append(this.attribute);
      builder.append("'");
      return builder.toString();
    }
  }

  @Override
  public int getPrecedence() {
    return 0;
  }

  public abstract Set<Value> getRange();
  public abstract DecoratedAttribute prime();
  public abstract DecoratedAttribute unprime();
}
