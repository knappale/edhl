package opspec;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import data.Attribute;
import edhl.Action;
import edhl.ControlState;
import edhl.DataConfiguration;
import edhl.Event;
import edhl.Formula;
import edhl.TransitionSystem;
import edhl.Transitions;
import util.Formatter;
import util.Pair;
import util.Relation;
import util.Sets;

import static java.util.stream.Collectors.toSet;

import java.util.HashSet;


@NonNullByDefault
public class OperationalSpecification {
  private Set<ControlState> controlStates;
  private Set<TransitionsSpecification> transitionRelationSpecification;
  private ControlState initialControlState;
  private data.Predicate initialPredicate;

  public OperationalSpecification(Set<TransitionsSpecification> transitionRelationSpecification, ControlState initialControlState, data.Predicate initialPredicate) {
    Relation<ControlState> relation = Relation.pairs(transitionRelationSpecification.stream().map(t -> new Pair<>(t.getSource(), t.getTarget())).collect(toSet()));
    this.controlStates = relation.reachables(Sets.singleton(initialControlState));
    this.transitionRelationSpecification = transitionRelationSpecification.stream().filter(t -> this.controlStates.contains(t.getSource()) && this.controlStates.contains(t.getTarget())).collect(toSet());
    this.initialControlState = initialControlState;
    this.initialPredicate = initialPredicate;
  }

  public Set<Event> getEvents() {
    return this.transitionRelationSpecification.stream().map(TransitionsSpecification::getEvent).collect(toSet());
  }

  public Set<data.Attribute> getAttributes() {
    return this.transitionRelationSpecification.stream().map(TransitionsSpecification::getAttributes).reduce(Sets.empty(), Sets::union);
  }

  public TransitionSystem getLargestSystem() {
    var attributes = this.getAttributes();
    var dataTransitions = new Transitions<Event, DataConfiguration>();
    for (Event event : this.getEvents()) {
      var eventTransitions = this.transitionRelationSpecification.stream().filter(t -> event.equals(t.getEvent())).map(t -> t.getLargestRelation(attributes)).reduce(Relation.empty(), Relation::union);
      dataTransitions.putRelation(event, eventTransitions);
    }

    var initials = this.initialPredicate.getValuations(attributes.stream().map(data.DecoratedAttribute::unprime).collect(toSet())).stream().map(v -> new DataConfiguration(this.initialControlState, v)).collect(toSet());

    return new TransitionSystem(dataTransitions, initials);
  }

  public Formula getCharacteristicFormula() {
    return new Formula.Bind(this.initialControlState.getVariable(),
                            Formula.and(Formula.data(this.initialPredicate),
                                        this.getCharacteristicFormula(this.initialControlState,
                                                                      this.transitionRelationSpecification.stream().filter(t -> this.initialControlState.equals(t.getSource())).collect(toSet()),
                                                                      this.controlStates,
                                                                      Sets.singleton(this.initialControlState))));
  }

  private Formula getCharacteristicFormula(ControlState c, Set<TransitionsSpecification> outgoings, Set<ControlState> states, Set<ControlState> bound) {
    if (!outgoings.isEmpty()) {
      TransitionsSpecification t = outgoings.iterator().next();
      outgoings = Sets.difference(outgoings, Sets.singleton(t));
      ControlState c1 = t.getTarget();
      if (bound.contains(c1))
        return new Formula.Jump(c.getVariable(),
                                Formula.implies(Formula.data(t.getPreCondition()),
                                                new Formula.Diamond(new Action(t.getEvent(), t.getPostCondition()),
                                                                    Formula.and(c1.getVariable(), this.getCharacteristicFormula(c, outgoings, states, bound)))));
      else
        return new Formula.Jump(c.getVariable(),
                                Formula.implies(Formula.data(t.getPreCondition()),
                                                new Formula.Diamond(new Action(t.getEvent(), t.getPostCondition()),
                                                                    new Formula.Bind(c1.getVariable(), this.getCharacteristicFormula(c, outgoings, states, Sets.union(bound, Sets.singleton(c1)))))));
    }
    states = Sets.difference(states, Sets.singleton(c));
    if (!states.isEmpty()) {
      ControlState c1 = Sets.intersection(states, bound).iterator().next();
      return Formula.and(finalise(c),
                         this.getCharacteristicFormula(c1, this.transitionRelationSpecification.stream().filter(t -> t.getSource().equals(c1)).collect(toSet()), states, bound));
    }
    return Formula.and(finalise(c),
                       this.controlStates.stream().map(c1 -> Sets.difference(this.controlStates, Sets.singleton(c1)).stream().
                                                   map(c2 -> Formula.differentControlStates(c1.getVariable(), c2.getVariable())).reduce(Formula.True(), Formula::and)).reduce(Formula.True(), Formula::and));
  }

  private Formula finalise(ControlState c) {
    return new Formula.Jump(c.getVariable(), this.getEvents().stream().map(e ->
                                                 Sets.powerSet(this.transitionRelationSpecification.stream().filter(t -> t.getSource().equals(c) && t.getEvent().equals(e)).collect(toSet())).stream().map(pp ->
                                                     just(c, e, pp)).reduce(Formula.True(), Formula::and)).reduce(Formula.True(), Formula::and));
  }

  private Formula just(ControlState c, Event e, Set<TransitionsSpecification> pp) {
    data.Predicate pred = data.Predicate.and(pp.stream().map(t -> t.getPreAndPostCondition()).reduce(data.Predicate.True(), data.Predicate::and),
                                                            data.Predicate.not(this.transitionRelationSpecification.stream().filter(t -> t.getSource().equals(c) && t.getEvent().equals(e) && !pp.contains(t)).map(t -> t.getPreAndPostCondition()).reduce(data.Predicate.False(), data.Predicate::or)));
    if (!pred.isSatisfiable(Attribute.getDecoratedAttributes(this.getAttributes())))
      return Formula.True();
    return new Formula.Box(new Action(e, pred),
                           pp.stream().map(t -> t.getTarget().getFormula()).reduce(Formula.False(), Formula::or));
  }

  public static OperationalSpecification product(OperationalSpecification left, OperationalSpecification right) {
    assert Sets.intersection(left.getAttributes(), right.getAttributes()).isEmpty();

    Set<TransitionsSpecification> transitionRelationSpecification = new HashSet<>();
    for (Event leftEvent : Sets.difference(left.getEvents(), right.getEvents()))
      for (TransitionsSpecification leftTransitionsSpecification : left.transitionRelationSpecification.stream().filter(trs -> trs.getEvent().equals(leftEvent)).collect(toSet()))
        for (ControlState rightControlState : right.controlStates)
          transitionRelationSpecification.add(leftTransitionsSpecification.leftProduct(rightControlState, right.getAttributes()));
    for (Event rightEvent : Sets.difference(right.getEvents(), left.getEvents()))
      for (TransitionsSpecification rightTransitionsSpecification : right.transitionRelationSpecification.stream().filter(trs -> trs.getEvent().equals(rightEvent)).collect(toSet()))
        for (ControlState leftControlState : left.controlStates)
          transitionRelationSpecification.add(rightTransitionsSpecification.leftProduct(leftControlState, left.getAttributes()));
    for (Event sharedEvent : Sets.intersection(left.getEvents(), right.getEvents()))
      for (TransitionsSpecification leftTransitionsSpecification : left.transitionRelationSpecification.stream().filter(trs -> trs.getEvent().equals(sharedEvent)).collect(toSet()))
        for (TransitionsSpecification rightTransitionsSpecification : right.transitionRelationSpecification.stream().filter(trs -> trs.getEvent().equals(sharedEvent)).collect(toSet()))
          transitionRelationSpecification.add(TransitionsSpecification.product(leftTransitionsSpecification, rightTransitionsSpecification));

    ControlState initialControlState = ControlState.product(left.initialControlState, right.initialControlState);
    data.Predicate initialPredicate = data.Predicate.and(left.initialPredicate, right.initialPredicate);

    return new OperationalSpecification(transitionRelationSpecification, initialControlState, initialPredicate);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("OperationalSpecification [\ncontrolStates=\n");
    builder.append(Formatter.separated(this.controlStates, "\n"));
    builder.append("\ntransitionRelationSpecification=\n");
    builder.append(Formatter.separated(this.transitionRelationSpecification, "\n"));
    builder.append("\ninitialControlState=");
    builder.append(this.initialControlState);
    builder.append("\ninitialPredicate=");
    builder.append(this.initialPredicate);
    builder.append("]");
    return builder.toString();
  }
}
