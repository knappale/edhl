package opspec;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;

import data.Predicate;
import edhl.ControlState;
import edhl.DataConfiguration;
import edhl.Event;
import util.Pair;
import util.Relation;
import util.Sets;

import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public class TransitionsSpecification {
  private ControlState source;
  private data.Predicate preCondition;
  private Event event;
  private data.Predicate postCondition;
  private ControlState target;

  public TransitionsSpecification(ControlState source, Predicate preCondition, Event event, Predicate postCondition, ControlState target) {
    super();
    this.source = source;
    this.preCondition = preCondition;
    this.event = event;
    this.postCondition = postCondition;
    this.target = target;
  }

  public ControlState getSource() {
    return this.source;
  }

  public data.Predicate getPreCondition() {
    return this.preCondition;
  }

  public Event getEvent() {
    return this.event;
  }

  public data.Predicate getPostCondition() {
    return this.postCondition;
  }

  public ControlState getTarget() {
    return this.target;
  }

  public Set<data.Attribute> getAttributes() {
    return Sets.union(this.preCondition.getAttributes(), this.postCondition.getAttributes());
  }

  public Relation<DataConfiguration> getLargestRelation(Set<data.Attribute> attributes) {
    Set<data.DecoratedAttribute> unprimeds = attributes.stream().map(data.DecoratedAttribute::unprime).collect(toSet());
    Set<data.Valuation> preValuations = this.preCondition.getValuations(unprimeds);

    Set<data.DecoratedAttribute> primeds = attributes.stream().map(data.DecoratedAttribute::prime).collect(toSet());
    Set<data.DecoratedAttribute> prePosts = Sets.union(unprimeds, primeds);
    Set<Pair<data.Valuation>> prePostValuations = this.postCondition.getValuations(prePosts).stream().filter(valuation -> this.preCondition.holdsAt(valuation)).map(valuation -> valuation.getPrePost()).collect(toSet());

    if (!prePostValuations.stream().map(prePost -> prePost.getFirst()).collect(toSet()).containsAll(preValuations))
      return Relation.empty();

    return Relation.pairs(prePostValuations.stream().map(prePost -> new Pair<>(new DataConfiguration(this.source, prePost.getFirst()),
                                                                               new DataConfiguration(this.target, prePost.getSecond()))).collect(toSet()));
  }

  public data.Predicate getPreAndPostCondition() {
    return data.Predicate.and(this.preCondition, this.postCondition);
  }

  public TransitionsSpecification leftProduct(ControlState controlState, Set<data.Attribute> attributes) {
    return new TransitionsSpecification(this.source.leftProduct(controlState), this.preCondition, this.event, data.Predicate.and(this.postCondition, new data.Predicate.Id(attributes)), this.target.leftProduct(controlState));
  }

  public TransitionsSpecification rightProduct(ControlState controlState, Set<data.Attribute> attributes) {
    return new TransitionsSpecification(this.source.rightProduct(controlState), this.preCondition, this.event, data.Predicate.and(this.postCondition, new data.Predicate.Id(attributes)), this.target.rightProduct(controlState));
  }

  public static TransitionsSpecification product(TransitionsSpecification left, TransitionsSpecification right) {
    assert left.event.equals(right.event);
    return new TransitionsSpecification(ControlState.product(left.source, right.source), data.Predicate.and(left.preCondition, right.preCondition), left.event, data.Predicate.and(left.postCondition, right.postCondition), ControlState.product(left.target, right.target));
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TransitionsSpecification [source=");
    builder.append(this.source);
    builder.append(", preCondition=");
    builder.append(this.preCondition);
    builder.append(", event=");
    builder.append(this.event);
    builder.append(", postCondition=");
    builder.append(this.postCondition);
    builder.append(", target=");
    builder.append(this.target);
    builder.append("]");
    return builder.toString();
  }
}
