package util;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;

import static java.util.stream.Collectors.toSet;


public class Relation<T> {
  private Set<Pair<T>> pairs = new HashSet<>();

  private Relation() {
  }

  private Relation(Set<Pair<T>> pairs) {
    this.pairs = pairs;
  }

  public static <T> @NonNull Relation<T> empty() {
    return new Relation<>();
  }

  public static <T> @NonNull Relation<T> pairs(Set<Pair<T>> pairs) {
    return new Relation<>(pairs);
  }

  public static <T> @NonNull Relation<T> union(Relation<T> left, Relation<T> right) {
    return new Relation<>(Sets.union(left.pairs, right.pairs));
  }
    
  public static <T> @NonNull Relation<T> intersection(Relation<T> left, Relation<T> right) {
    return new Relation<>(Sets.intersection(left.pairs, right.pairs));
  }
    
  public static <T> @NonNull Relation<T> seq(Relation<T> left, Relation<T> right) {
    Set<Pair<T>> seqPairs = new HashSet<>();
    for (Pair<T> first : left.pairs) {
      for (Pair<T> second : right.pairs) {
        if (Objects.equals(first.getSecond(), second.getFirst()))
          seqPairs.add(new Pair<>(first.getFirst(), second.getSecond()));
      }
    }
    return new Relation<>(seqPairs);
  }

  public static <T> @NonNull Relation<T> reflexive(Set<T> elements) {
    return new Relation<>(elements.stream().map(e -> new Pair<>(e, e)).collect(toSet()));
  }

  public static <T> @NonNull Relation<T> transitive(Relation<T> relation) {
    Set<Pair<T>> transitivePairs = relation.pairs;
    while (true) {
      Set<Pair<T>> newTransitivePairs = Sets.union(transitivePairs, seq(new Relation<>(transitivePairs), relation).pairs);
      if (newTransitivePairs.equals(transitivePairs))
        break;
      transitivePairs = newTransitivePairs;
    }
    return new Relation<>(transitivePairs);
  }

  public @NonNull Set<T> successors(Set<T> elements) {
    return this.pairs.stream().filter(p -> elements.contains(p.getFirst())).map(p -> p.getSecond()).collect(toSet());
  }

  public @NonNull Set<T> predecessors(Set<T> elements) {
    return this.pairs.stream().filter(p -> elements.contains(p.getSecond())).map(p -> p.getFirst()).collect(toSet());
  }

  public @NonNull Set<T> reachables(Set<T> elements) {
    Set<T> reachables = new HashSet<>(elements);
    while (true) {
      Set<T> nextReachables = new HashSet<>(reachables);
      nextReachables.addAll(this.successors(reachables));
      if (reachables.equals(nextReachables))
        break;
      reachables = nextReachables;
    }
    return reachables;
  }

  public @NonNull Set<T> domain() {
    return this.pairs.stream().map(p -> p.getFirst()).collect(toSet());
  }

  public @NonNull Set<T> codomain() {
    return this.pairs.stream().map(p -> p.getSecond()).collect(toSet());
  }

  public @NonNull Relation<T> domainRestrict(Set<T> elements) {
    return new Relation<>(this.pairs.stream().filter(p -> elements.contains(p.getFirst())).collect(toSet()));
  }

  /**
   * Apply a function to all elements of this relation.
   *
   * @param <U> result type
   * @param fun function
   * @return result relation of applying {@code fun} on all elements
   */
  public <U> @NonNull Relation<U> apply(Function<T, U> fun) {
    return new Relation<>(this.pairs.stream().map(p -> p.apply(fun)).collect(toSet()));
  }

  public int size() {
    return this.pairs.size();
  }

  public boolean add(Pair<T> pair) {
    return this.pairs.add(pair);
  }

  public boolean addAll(Relation<T> relation) {
    return this.pairs.addAll(relation.pairs);
  }

  public Stream<Pair<T>> stream() {
    return this.pairs.stream();
  }

  public static <T> Stream<Pair<T>> toStream(Relation<T> relation) {
    if (relation == null)
      return Stream.empty();
    return relation.stream();
  }

  public static <T> Collector<Pair<T>, ?, @NonNull Relation<T>> toRelation() {
    return Collector.of((Supplier<@NonNull Relation<T>>)Relation::new,
                        Relation::add,
                        (left, right) -> {
                                           if (left.size() < right.size()) {
                                             right.addAll(left); return right;
                                           }
                                           else {
                                             left.addAll(right); return left;
                                           }
                                         });
  }

  @Override
  public int hashCode() {
    return this.pairs.hashCode();
  }

  @Override
  public boolean equals(Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    if (this.getClass() != object.getClass())
      return false;
    Relation<?> other = (Relation<?>)object;
    return Objects.equals(this.pairs, other.pairs);
  }

  @Override
  public String toString() {
    return Formatter.set(this.pairs);
  }
}
