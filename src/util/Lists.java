package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


/**
 * Auxiliary list functions
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
public class Lists {
  /**
   * Compute an empty list.
   *
   * @return { }
   */
  public static <T> List<T> empty() {
    return new ArrayList<>();
  }

  /**
   * Compute a list containing some elements.
   *
   * @return [ {@code e}1, ..., {@code e}n ]
   */
  @SafeVarargs
  public static <T> List<T> elements(T... e) {
    return Arrays.asList(e);
  }

  /**
   * Compute the concatenation of two lists.
   *
   * @param list1 a list
   * @param list2 a list
   * @return append({@code list1}, {@code list2})
   */
  public static <T> List<T> append(List<T> list1, List<T> list2) {
    List<T> result = new ArrayList<>();
    result.addAll(list1);
    result.addAll(list2);
    return result;
  }

  /**
   * Compute the last element of a non-empty list.
   *
   * @param list a list
   * @pre !list.isEmpty()
   * @return the last element of the list
   */
  public static <T> T last(List<T> list) {
    return list.get(list.size()-1);
  }

  /**
   * Given a list of object lists return the concatenation product.
   * 
   * Example: product([[a, b], [c], [d, e]]) =
   *            [[a, c, d], [a, c, e], [b, c, d], [b, c, e]]
   *
   * @param lists a list of object lists
   * @return a list of lists of objects
   */
  public static <T> List<List<T>> product(List<List<T>> objectsLists) {
    List<List<T>> productLists = new ArrayList<List<T>>();

    if (objectsLists.size() == 0) {
      productLists.add(new ArrayList<T>());
      return productLists;
    }

    List<T> objects = objectsLists.get(objectsLists.size()-1);
    final List<List<T>> restObjectLists = objectsLists.subList(0, objectsLists.size()-1);
    assert (restObjectLists != null);
    for (Iterator<List<T>> remainingProductListsIt = product(restObjectLists).iterator(); remainingProductListsIt.hasNext(); ) {
      List<T> remainingProductList = remainingProductListsIt.next();
      for (Iterator<T> objectsIt = objects.iterator(); objectsIt.hasNext(); ) {
        T object = objectsIt.next();
        List<T> productList = new ArrayList<T>();
        productList.addAll(remainingProductList);
        productList.add(object);
        productLists.add(productList);
      }
    }

    return productLists;
  }

  /**
   * Given a list of lists of lists return the concatenation product.
   *
   * Example: mergings([[[a, b]], [[c]], [[d], [e, f]]]) =
   *            [[a, b, c, d], [a, b, c, e, f]]
   *
   * Mergings can be alternatively produced by:
   *   (map flatten) (product(matrices))
   *
   * @param matrices a list of lists of lists
   * @return concatenation product of list of lists of lists
   */
  public static <T> List<List<T>> mergings(List<List<List<T>>> matrices) {
    List<List<T>> mergings = new ArrayList<List<T>>();

    if (matrices.size() == 0) {
      List<T> merging = new ArrayList<T>();
      mergings.add(merging);
      return mergings;
    }

    List<List<T>> firstLists = matrices.get(0);
    final List<List<List<T>>> restMatrices = matrices.subList(1, matrices.size());
    assert (restMatrices != null);
    for (Iterator<List<T>> restListsIt = mergings(restMatrices).iterator(); restListsIt.hasNext(); ) {
      List<T> restList = restListsIt.next();
      for (Iterator<List<T>> firstListsIt = firstLists.iterator(); firstListsIt.hasNext(); ) {
        List<T> firstList = firstListsIt.next();
        List<T> merging = new ArrayList<T>();
        merging.addAll(firstList);
        merging.addAll(restList);
        mergings.add(merging);
      }
    }
    return mergings;
  }

  /**
   * Given a list of objects return a list of all permutations.
   *
   * @param list of objects
   * @return a list of permutations of the list
   */
  public static <T> List<List<T>> permutations(List<T> list) {
    List<List<T>> permutations = new ArrayList<>();

    if (list.size() <= 1) {
      permutations.add(list);
      return permutations;
    }

    T firstElement = list.get(0);
    List<T> subList = list.subList(1, list.size());
    assert (subList != null);
    for (Iterator<List<T>> restPermutationsIt = permutations(subList).iterator(); restPermutationsIt.hasNext(); ) {
      List<T> restPermutation = restPermutationsIt.next();
      int restPermutationSize = restPermutation.size();
      for (int i = 0; i <= restPermutation.size(); i++) {
        List<T> restPermutationLeft = restPermutation.subList(0, i);
        List<T> restPermutationRight = restPermutation.subList(i, restPermutationSize);
        List<T> permutation = new ArrayList<>();
        permutation.addAll(restPermutationLeft);
        permutation.add(firstElement);
        permutation.addAll(restPermutationRight);
        permutations.add(permutation);
      }
    }
    return permutations;
  }

  /**
   * Topological sorting of a list w.r.t. a comparator.
   *
   * @param list a list
   * @param comparator a comparator
   * @return topologically sorted list w.r.t. {@code comparator}
   */
  public static <T> List<T> topologicalSort(List<T> list, Comparator<? super T> comparator) {
    if (list.size() == 0)
      return list;

    // Find a minimal element
    int minimalIndex = -1;
    for (int i = 0; minimalIndex < 0 && i < list.size(); i++) {
      T element1 = list.get(i);
      boolean minimal = true;
      for (int j = 0; minimal && j < list.size(); j++) {
        T element2 = list.get(j);
        minimal &= (comparator.compare(element1, element2) <= 0);
      }
      if (minimal)
        minimalIndex = i;
    }

    List<T> subList = new ArrayList<>();
    subList.addAll(list.subList(0, minimalIndex));
    if (minimalIndex < list.size()-1)
      subList.addAll(list.subList(minimalIndex+1, list.size()));
    List<T> sortedList = new ArrayList<>();
    sortedList.add(list.get(minimalIndex));
    sortedList.addAll(topologicalSort(subList, comparator));
    return sortedList;
  }

  public static <T> boolean containsByIdentity(List<T> list, T item) {
    for (T elem : list)
      if (elem == item)
        return true;
    return false;
  }
}
