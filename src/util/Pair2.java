package util;

import java.util.Objects;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNull;


/**
 * Pairs.
 *
 * @author <A HREF="mailto:knapp@pst.ifi.lmu.de>Alexander Knapp</A>
 */
public class Pair2<A, B> {
  private A first;
  private B second;

  /**
   * Create a new pair.
   *
   * @param first the first component
   * @param second the second component
   */
  public Pair2(A first, B second) {
    this.first = first;
    this.second = second;
  }

  /**
   * @return this pair's first component
   */
  public A getFirst() {
    return this.first;
  }

  /**
   * @return this pair's second component
   */
  public B getSecond() {
    return this.second;
  }

  /**
   * Apply functions on both components.
   *
   * @param <X> result type for first component
   * @param <Y> result type for second component
   * @param firstFun function for first component
   * @param secondFun function for second component
   * @return result of applying {@code firstFun} on the first and {@code secondFun} on the second component
   */
  public <X, Y> @NonNull Pair2<X, Y> apply(Function<A, X> firstFun, Function<B, Y> secondFun) {
    return new Pair2<>(firstFun.apply(this.first), secondFun.apply(this.second));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.first, this.second);
  }

  @Override
  public boolean equals(Object object) {
    if (object == null)
      return false;
    try {
      Pair2<?, ?> other = (Pair2<?, ?>)object;
      return Objects.equals(this.first, other.first) &&
             Objects.equals(this.second, other.second);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("<");
    builder.append(this.first);
    builder.append(", ");
    builder.append(this.second);
    builder.append(">");
    return builder.toString();
  }
}
