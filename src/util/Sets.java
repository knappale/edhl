package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNull;

import static java.util.stream.Collectors.toSet;


/**
 * Auxiliary set functions
 *
 * @author <A HREF="mailto:knapp@informatik.uni-muenchen.de">Alexander Knapp</A>
 */
public class Sets {
  /**
   * Compute an empty set.
   *
   * @return { }
   */
  public static <T> @NonNull Set<T> empty() {
    return new HashSet<>();
  }

  /**
   * Compute a singleton set.
   *
   * @param e an element
   * @return { {@code e} }
   */
  public static <T> @NonNull Set<T> singleton(T e) {
    Set<T> singleton = new HashSet<>();
    singleton.add(e);
    return singleton;
  }

  /**
   * Compute a set containing some elements.
   *
   * @return { {@code e}1, ..., {@code e}n }
   */
  @SafeVarargs
  public static <T> @NonNull Set<T> elements(T... e) {
    Set<T> set = new HashSet<>();
    set.addAll(Arrays.asList(e));
    return set;
  }

  /**
   * Compute the union of two sets.
   *
   * @param set1 a set
   * @param set2 a set
   * @return {@code set1} \cup {@code set2}
   */
  public static <T> @NonNull Set<T> union(Set<T> set1, Set<T> set2) {
    Set<T> union = new HashSet<>();
    union.addAll(set1);
    union.addAll(set2);
    return union;
  }

  /**
   * Compute the intersection of two sets.
   *
   * @param set1 a set
   * @param set2 a set
   * @return {@code set1} \cap {@code set2}
   */
  public static <T> @NonNull Set<T> intersection(Set<T> set1, Set<T> set2) {
    return set1.stream().filter(e -> set2.contains(e)).collect(toSet());
  }

  /**
   * Compute the set difference between two sets.
   *
   * @param set1 a set
   * @param set2 a set
   * @return {@code set1} \ {@code set2}
   */
  public static <T> @NonNull Set<T> difference(Set<T> set1, Set<T> set2) {
    return set1.stream().filter(e -> !set2.contains(e)).collect(toSet());
  }

  /**
   * Compute the power set of a set.
   *
   * @param set a set
   * @return the powerset of {@code set}
   */
  public static <T> @NonNull Set<@NonNull Set<T>> powerSet(Set<T> set) {
    Set<@NonNull Set<T>> powerSet = new HashSet<>();

    if (set.isEmpty()) {
      powerSet.add(new HashSet<>());
      return powerSet;
    }

    T element = set.iterator().next();
    Set<T> set1 = new HashSet<>();
    set1.addAll(set);
    set1.remove(element);
    Set<@NonNull Set<T>> powerSet1 = powerSet(set1);
    powerSet.addAll(powerSet1);
    for (java.util.Set<T> subSet1 : powerSet1) {
      Set<T> subSet2 = new HashSet<>();
      subSet2.addAll(subSet1);
      subSet2.add(element);
      powerSet.add(subSet2);
    }
    return powerSet;
  }

  /**
   * Determine whether two sets are disjoint.
   *
   * @param set1 a set
   * @param set2 another set
   * @return whether {@code set1} and {@code set2} are disjoint
   */
  public static <T> boolean isDisjoint(Set<T> set1, Set<T> set2) {
    for (T element1 : set1) {
      if (set2.contains(element1))
	return false;
    }
    return true;
  }

  public static <T> @NonNull Set<@NonNull Set<T>> unionProduct(Set<Set<Set<T>>> sets) {
    Set<@NonNull Set<T>> unionProduct = new HashSet<>();

    List<List<Set<T>>> list = new ArrayList<>();
    for (Set<Set<T>> set : sets) {
      list.add(new ArrayList<Set<T>>(set));
    }
    List<List<Set<T>>> productLists = Lists.product(list);
    for (List<Set<T>> productList : productLists) {
      Set<T> productSet = new HashSet<>();
      for (Set<T> set : productList)
	productSet.addAll(set);
      unionProduct.add(productSet);
    }
    return unionProduct;
  }
}
