package util;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;

import org.eclipse.jdt.annotation.NonNull;


public class Formatter {
  /**
   * @param item an item
   * @param map a map extracting information
   * @return item's string representation using {@code map} if item is non-null
   */
  public static <T> @NonNull String plain(final T item, @NonNull Function<@NonNull T, String> map) {
    if (item == null)
      return "null";
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(map.apply(item));
    return resultBuilder.toString();
  }

  /**
   * @param item an item
   * @return item's string representation, surround by quotes
   */
  public static <T> @NonNull String quoted(T item) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("`");
    resultBuilder.append(item);
    resultBuilder.append("'");
    return resultBuilder.toString();
  }

  /**
   * @param item an item
   * @return item's string representation, surrounded with parentheses
   */
  public static <T> @NonNull String parenthesised(final T item) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("(");
    resultBuilder.append(item);
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  public static <T> @NonNull String separated(Collection<T> items, @NonNull Function<T, String> map, String separator) {
    StringBuilder builder = new StringBuilder();
    String sep = "";
    for (T item : items) {
      builder.append(sep);
      builder.append(map.apply(item));
      sep = separator;
    }
    return builder.toString();
  }

  public static <T> @NonNull String separated(Collection<T> items, String separator) {
    return separated(items, (item -> (item == null) ? "null" : item.toString()), separator);
  }

  public static <T> @NonNull String set(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    if (items.isEmpty())
      return "{ }";
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("{ ");
    resultBuilder.append(separated(items, map, ", "));
    resultBuilder.append(" }");
    return resultBuilder.toString();
  }

  public static <T> @NonNull String set(Collection<T> items) {
    return set(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <T> @NonNull String setOrSingleton(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    if (items.size() == 1) {
      String itemString = map.apply(items.iterator().next());
      if (itemString == null)
        return "";
      return itemString;
    }
    return set(items, map);
  }

  public static <T> @NonNull String setOrSingleton(Collection<T> items) {
    return setOrSingleton(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <T> @NonNull String list(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("[");
    resultBuilder.append(separated(items, map, ", "));
    resultBuilder.append("]");
    return resultBuilder.toString();
  }

  public static <T> @NonNull String list(Collection<T> items) {
    return list(items, item -> (item == null) ? "null" : item.toString());
  }

  public static <T> @NonNull String tuple(Collection<T> items, @NonNull Function<T, String> map) {
    if (items == null)
      return "";
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("(");
    resultBuilder.append(separated(items, map, ", "));
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  public static <K, V> @NonNull String map(Map<K, V> items, @NonNull Function<K, String> keyMap, @NonNull Function<V, String> valueMap) {
    if (items == null)
      return "";
    if (items.isEmpty())
      return "{ }";
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("{ ");
    resultBuilder.append(separated(items.entrySet(), (e -> keyMap.apply(e.getKey()) + " = " + valueMap.apply(e.getValue())), ", "));
    resultBuilder.append(" }");
    return resultBuilder.toString();
  }

  public static <K, V> @NonNull String map(Map<K, V> items) {
    return map(items, k -> k == null ? "null" : k.toString(), v -> v == null ? "null" : v.toString());
  }

  public static <T> @NonNull String tuple(Collection<T> items) {
    return tuple(items, item -> (item == null) ? "null" : item.toString());
  }

  /**
   * @param item an item
   * @param hasPrecedence test whether the item takes precedence
   * @param map yielding item's string representation
   * @return item's string representation, surrounded with parentheses
   *         if {@code hasPrecedence.test(item)} is false
   */
  public static <T> @NonNull String parenthesised(final T item, @NonNull Predicate<T> hasPrecedence, Function<T, String> map) {
    if (hasPrecedence.test(item)) {
      String representation = map.apply(item);
      if (representation == null)
        return "null";
      return representation;
    }

    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append("(");
    resultBuilder.append(map.apply(item));
    resultBuilder.append(")");
    return resultBuilder.toString();
  }

  /**
   * @param item an item
   * @param hasPrecedence test whether the item takes precedence
   * @return item's string representation, surrounded with parentheses
   *         if {@code hasPrecedence.test(item)} is false
   */
  public static <T> @NonNull String parenthesised(final T item, @NonNull Predicate<T> hasPrecedence) {
    return parenthesised(item, hasPrecedence, i -> (i == null) ? "null" : i.toString());
  }

  public static <T, U> @NonNull String type(final T item, final U type) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(quoted(item));
    resultBuilder.append(" : ");
    resultBuilder.append(quoted(type));
    return resultBuilder.toString();
  }

  public static <T, U> @NonNull String context(final T item, final U context) {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(quoted(item));
    resultBuilder.append(" in context ");
    resultBuilder.append(quoted(context));
    return resultBuilder.toString();
  }
}
