package util;

import java.util.function.Function;
import java.util.stream.Stream;

import org.eclipse.jdt.annotation.NonNull;


/**
 * Pairs.
 *
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de>Alexander Knapp</A>
 */
public class Pair<T> extends Pair2<T, T>{
  /**
   * Create a new pair.
   *
   * @param first the first component
   * @param second the second component
   */
  public Pair(T first, T second) {
    super(first, second);
  }

  /**
   * Apply functions on both components.
   *
   * @param <U> result type for both components
   * @param fun function for both components
   * @return result pair of applying {@code fun} on the first and the second component
   */
  public <U> @NonNull Pair<U> apply(Function<T, U> fun) {
    return new Pair<>(fun.apply(this.getFirst()), fun.apply(this.getSecond()));
  }

  /**
   * Apply functions on both components.
   *
   * @param <U> result type for both component
   * @param firstFun function for first component
   * @param secondFun function for second component
   * @return result of applying {@code firstFun} on the first and {@code secondFun} on the second component
   */
  public <U> @NonNull Pair<U> apply2(Function<T, U> firstFun, Function<T, U> secondFun) {
    return new Pair<>(firstFun.apply(this.getFirst()), secondFun.apply(this.getSecond()));
  }

  public Stream<T> stream() {
    return Stream.of(this.getFirst(), this.getSecond());
  }
}
