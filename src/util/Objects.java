package util;

import org.eclipse.jdt.annotation.NonNull;
import org.eclipse.jdt.annotation.Nullable;


/**
 * Utilities for objects.
 * 
 * @author <A HREF="mailto:knapp@informatik.uni-augsburg.de">Alexander Knapp</A>
 */
public final class Objects {
  public static @NonNull <T> T assertNonNull(@Nullable T t) {
    assert (t != null) : "@NonNull invariant violated";
    return t;
  }
}
