package edhl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class DataConfiguration {
  private static Map<DataConfiguration, Configuration> configurationsMap = new HashMap<>();

  private ControlState controlState;
  private data.Valuation dataLabel;

  public DataConfiguration(ControlState controlState, data.Valuation dataLabel) {
    this.controlState = controlState;
    this.dataLabel = dataLabel;
  }

  public ControlState getControlState() {
    return this.controlState;
  }

  public data.Valuation getDataLabel() {
    return this.dataLabel;
  }

  public Configuration getConfiguration() {
    var configuration = configurationsMap.get(this);
    if (configuration == null) {
      configuration = new Configuration(this.controlState);
      configurationsMap.put(this, configuration);
    }
    return configuration;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.controlState,
                        this.dataLabel);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      DataConfiguration other = (DataConfiguration)object;
      return Objects.equals(this.controlState, other.controlState) &&
             Objects.equals(this.dataLabel, other.dataLabel);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Configuration [controlState=");
    builder.append(this.controlState);
    builder.append(", dataLabel=");
    builder.append(this.dataLabel);
    builder.append("]");
    return builder.toString();
  }
}
