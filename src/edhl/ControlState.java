package edhl;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class ControlState {
  private String name;

  public ControlState(String name) {
    this.name = name;
  }

  public Variable getVariable() {
    return new Variable(this.name);
  }

  public Formula getFormula() {
    return new Variable(this.name);
  }

  public ControlState leftProduct(ControlState other) {
    return product(this, other);
  }

  public ControlState rightProduct(ControlState other) {
    return product(other, this);
  }

  public static ControlState product(ControlState left, ControlState right) {
    return new ControlState("(" + left.name + ", " + right.name + ")");
  }

  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      ControlState other = (ControlState)object;
      return Objects.equals(this.name, other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.name;
  }
}
