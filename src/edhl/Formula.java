package edhl;

import static java.util.stream.Collectors.toSet;

import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Sets;


@NonNullByDefault
public abstract class Formula {
  private static Formula FALSE = new False();
  private static Formula TRUE = new True();

  public static class Data extends Formula {
    private data.Predicate predicate;

    public Data(data.Predicate predicate) {
      this.predicate = predicate;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return system.getConfigurations().stream().filter(c -> system.getDataLabel(c).satisfies(this.predicate)).collect(toSet());
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.predicate);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (this.getClass() != object.getClass())
        return false;
      Data other = (Data)object;
      return Objects.equals(this.predicate, other.predicate);
    }

    @Override
    public String toString() {
      return this.predicate.toString();
    }
  }

  public static class Bind extends Formula {
    private Variable variable;
    private Formula formula;

    public Bind(Variable variable, Formula formula) {
      this.variable = variable;
      this.formula = formula;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return system.getControlStates().stream().map(c -> this.formula.satisfactionSet(system, valuation.with(this.variable, c))).reduce(Sets.empty(), Sets::union);
    }

    @Override
    public int getPrecedence() {
      return 14;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.formula,
                          this.variable);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Bind other = (Bind)object;
      return Objects.equals(this.formula, other.formula)
          && Objects.equals(this.variable, other.variable);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("|");
      builder.append(this.variable);
      builder.append(" . ");
      builder.append(this.formula);
      return builder.toString();
    }
  }

  public static class Jump extends Formula {
    private Variable variable;
    private @Nullable Set<Event> events;
    private Formula formula;

    public Jump(Variable variable, @Nullable Set<Event> events, Formula formula) {
      this.variable = variable;
      this.events = events;
      this.formula = formula;
    }

    public Jump(Variable variable, Formula formula) {
      this(variable, null, formula);
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      Set<Event> events = this.events != null ? this.events : system.getEvents();
      Set<Configuration> targets = system.getConfigurations(events).stream().filter(c -> c.getControlState().equals(valuation.getControlState(this.variable))).collect(toSet());
      if (this.formula.satisfactionSet(system, valuation).containsAll(targets))
        return system.getConfigurations();
      return Sets.empty();
    }

    @Override
    public int getPrecedence() {
      return 14;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.variable,
                          this.events,
                          this.formula);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Jump other = (Jump)object;
      return Objects.equals(this.variable, other.variable)
          && Objects.equals(this.events, other.events)
          && Objects.equals(this.formula, other.formula);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("(@");
      if (this.events != null) {
        builder.append(Formatter.set(this.events));
        builder.append(" ");
      }
      builder.append(this.variable);
      builder.append(")");
      builder.append(this.formula);
      return builder.toString();
    }
  }

  public static class Diamond extends Formula {
    private Label label;
    private Formula formula;

    public Diamond(Label label, Formula formula) {
      this.label = label;
      this.formula = formula;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return system.getRelation(this.label).predecessors(this.formula.satisfactionSet(system, valuation));
    }

    @Override
    public int getPrecedence() {
      return 3;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.label,
                          this.formula);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Diamond other = (Diamond)object;
      return Objects.equals(this.label, other.label)
          && Objects.equals(this.formula, other.formula);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("<");
      builder.append(this.label);
      builder.append(">");
      builder.append(this.formula);
      return builder.toString();
    }
  }

  public static class Box extends Formula {
    private Label label;
    private Formula formula;

    public Box(Label label, Formula formula) {
      this.label = label;
      this.formula = formula;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return Sets.difference(system.getConfigurations(), new Diamond(this.label, new Not(this.formula)).satisfactionSet(system, valuation));
    }

    @Override
    public int getPrecedence() {
      return 3;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.label,
                          this.formula);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Box other = (Box)object;
      return Objects.equals(this.label, other.label)
          && Objects.equals(this.formula, other.formula);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("[");
      builder.append(this.label);
      builder.append("]");
      builder.append(this.formula);
      return builder.toString();
    }
  }

  private static class False extends Formula {
    public False() {
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return Sets.empty();
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return (this.getClass() == object.getClass());
    }

    @Override
    public String toString() {
      return "false";
    }
  }
    
  private static class True extends Formula {
    public True() {
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return system.getConfigurations();
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return (this.getClass() == object.getClass());
    }

    @Override
    public String toString() {
      return "true";
    }
  }
    
  private static class Not extends Formula {
    private Formula formula;

    public Not(Formula formula) {
      this.formula = formula;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return Sets.difference(system.getConfigurations(), this.formula.satisfactionSet(system, valuation));
    }

    @Override
    public int getPrecedence() {
      return 2;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.formula);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Not other = (Not)object;
        return Objects.equals(this.formula, other.formula);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append("!");
      resultBuilder.append(Formatter.parenthesised(this.formula, f -> f.hasPrecedence(this)));
      return resultBuilder.toString();
    }
  }

  private static class And extends Formula {
    private Formula left, right;

    public And(Formula left, Formula right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return Sets.intersection(this.left.satisfactionSet(system, valuation), this.right.satisfactionSet(system, valuation));
    }

    @Override
    public int getPrecedence() {
      return 11;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        And other = (And)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      resultBuilder.append(" && ");
      resultBuilder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return resultBuilder.toString();
    }
  }

  private static class Or extends Formula {
    private Formula left, right;

    public Or(Formula left, Formula right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return Sets.union(this.left.satisfactionSet(system, valuation), this.right.satisfactionSet(system, valuation));
    }

    @Override
    public int getPrecedence() {
      return 12;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Or other = (Or)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      resultBuilder.append(" || ");
      resultBuilder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return resultBuilder.toString();
    }
  }

  private static class Implies extends Formula {
    private Formula left, right;

    public Implies(Formula left, Formula right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
      return Sets.union(Sets.difference(system.getConfigurations(), this.left.satisfactionSet(system, valuation)), this.right.satisfactionSet(system, valuation));
    }

    @Override
    public int getPrecedence() {
      return 13;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (object == null)
        return false;
      try {
        Implies other = (Implies)object;
        return Objects.equals(this.left, other.left)
            && Objects.equals(this.right, other.right);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder resultBuilder = new StringBuilder();
      resultBuilder.append(Formatter.parenthesised(this.left, f -> f.hasPrecedence(this)));
      resultBuilder.append(" -> ");
      resultBuilder.append(Formatter.parenthesised(this.right, f -> f.hasPrecedence(this)));
      return resultBuilder.toString();
    }
  }

  public abstract int getPrecedence();

  public boolean hasPrecedence(Formula other) {
    return this.getPrecedence() <= other.getPrecedence();
  }

  public static Formula data(data.Predicate dataPredicate) {
    if (dataPredicate.equals(data.Predicate.False()))
      return FALSE;
    if (dataPredicate.equals(data.Predicate.True()))
      return TRUE;
    return new Formula.Data(dataPredicate);
  }

  public static Formula False() {
    return FALSE;
  }

  public static Formula True() {
    return TRUE;
  }

  public static Formula not(Formula formula) {
    if (formula.equals(FALSE))
      return TRUE;
    if (formula.equals(TRUE))
      return FALSE;
    return new Not(formula);
  }

  public static Formula and(Formula left, Formula right) {
    if (left.equals(FALSE) || right.equals(FALSE))
      return FALSE;
    if (left.equals(TRUE))
      return right; 
    if (right.equals(TRUE))
      return left;
    if (left.equals(right))
      return left;
    return new And(left, right);
  }

  public static Formula and(Set<Formula> formulae) {
    return formulae.stream().reduce(TRUE, Formula::and);
  }

  public static Formula or(Formula left, Formula right) {
    if (left.equals(TRUE) || right.equals(TRUE))
      return TRUE;
    if (left.equals(FALSE))
      return right; 
    if (right.equals(FALSE))
      return left;
    if (left.equals(right))
      return left;
    return new Or(left, right);
  }

  public static Formula or(Set<Formula> formulae) {
    return formulae.stream().reduce(FALSE, Formula::or);
  }

  public static Formula implies(Formula left, Formula right) {
    if (left.equals(FALSE) || right.equals(TRUE))
      return TRUE;
    if (left.equals(TRUE))
      return right; 
    if (right.equals(FALSE))
      return new Not(left);
    if (left.equals(right))
      return TRUE;
    return new Implies(left, right);
  }

  public static Formula differentControlStates(Variable v1, Variable v2) {
    return new Not(new Jump(v1, v2));
  }

  public abstract Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation);

  public Set<Configuration> satisfactionSet(TransitionSystem system) {
    return this.satisfactionSet(system, new Valuation());
  }
}
