package edhl;

import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Relation;
import util.Sets;


@NonNullByDefault
public abstract class CompositeEvent {
  public static class Not extends CompositeEvent {
    private Set<Event> events;

    public Not(Event event) {
      this.events = Sets.singleton(event);
    }

    public Not(Set<Event> events) {
      this.events = events;
    }

    @Override
    public int getPrecedence() {
      return 2;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Sets.difference(system.getEvents(), this.events).stream().
                 map(event -> system.getRelation(event)).reduce(Relation.empty(), Relation::union);
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.events);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      try {
        Not other = (Not)object;
        return Objects.equals(this.events, other.events);
      }
      catch (ClassCastException cce) {
        return false;
      }
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append("-");
      builder.append(Formatter.setOrSingleton(this.events));
      return builder.toString();
    }
  }

  public static class All extends CompositeEvent {
    public All() {
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return system.getEvents().stream().
                 map(event -> system.getRelation(event)).reduce(Relation.empty(), Relation::union);
    }

    @Override
    public int getPrecedence() {
      return 0;
    }

    @Override
    public int hashCode() {
      return 0;
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      return this.getClass() == object.getClass();
    }

    @Override
    public String toString() {
      return "EE";
    }
  }

  public static class Choice extends CompositeEvent {
    private CompositeEvent left, right;

    public Choice(CompositeEvent left, CompositeEvent right) {
      this.left = left;
      this.right = right;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Relation.union(this.left.interpretOver(system), this.right.interpretOver(system));
    }

    @Override
    public int getPrecedence() {
      return 12;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Choice other = (Choice)object;
      return Objects.equals(left, other.left)
          && Objects.equals(right, other.right);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, e -> e.hasPrecedence(this)));
      builder.append(" + ");
      builder.append(Formatter.parenthesised(this.right, e -> e.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Seq extends CompositeEvent {
    private CompositeEvent left, right;
 
    public Seq(CompositeEvent left, CompositeEvent right) {
      super();
      this.left = left;
      this.right = right;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Relation.seq(this.left.interpretOver(system), this.right.interpretOver(system));
    }

    @Override
    public int getPrecedence() {
      return 15;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Seq other = (Seq)object;
      return Objects.equals(left, other.left)
          && Objects.equals(right, other.right);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, l -> l.hasPrecedence(this)));
      builder.append("; ");
      builder.append(Formatter.parenthesised(this.right, l -> l.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Count extends CompositeEvent {
    private int count;
    private CompositeEvent compositeEvent;

    public Count(int count, CompositeEvent compositeEvent) {
      this.count = count;
      this.compositeEvent = compositeEvent;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      Relation<Configuration> relation = Relation.reflexive(system.getConfigurations());
      for (int n = 0; n < this.count; n++)
        relation = Relation.seq(relation, this.compositeEvent.interpretOver(system));
      return relation;
    }

    @Override
    public int getPrecedence() {
      return 3;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.count, this.compositeEvent);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Count other = (Count)object;
      return Objects.equals(this.count, other.count)
          && Objects.equals(this.compositeEvent, other.compositeEvent);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.compositeEvent, l -> l.hasPrecedence(this)));
      builder.append("^");
      builder.append(this.count);
      return builder.toString();
    }
  }

  public static class Iterate extends CompositeEvent {
    private CompositeEvent compositeEvent;

    public Iterate(CompositeEvent compositeEvent) {
      super();
      this.compositeEvent = compositeEvent;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Relation.transitive(Relation.union(Relation.reflexive(system.getConfigurations()), this.compositeEvent.interpretOver(system)));
    }

    @Override
    public int getPrecedence() {
      return 3;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.compositeEvent);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Iterate other = (Iterate)object;
      return Objects.equals(this.compositeEvent, other.compositeEvent);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.compositeEvent, l -> l.hasPrecedence(this)));
      builder.append("*");
      return builder.toString();
    }
  }

  public abstract int getPrecedence();

  public boolean hasPrecedence(CompositeEvent other) {
    return other.getPrecedence() > this.getPrecedence();
  }

  public abstract Relation<Configuration> interpretOver(TransitionSystem system);
}
