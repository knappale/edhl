package edhl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Relation;
import static util.Relation.toRelation;
import static util.Relation.toStream;
import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public class Transitions<T, U> {
  private Map<T, Relation<U>> relationsMap;

  public Transitions() {
    this.relationsMap = new HashMap<>();
  }

  public Transitions(Map<T, Relation<U>> transitionsMap) {
    this.relationsMap = transitionsMap;
  }

  public Relation<U> getRelation(T t) {
    var relation = this.relationsMap.get(t);
    if (relation == null)
      return Relation.empty();
    return relation;
  }

  public void putRelation(T t, Relation<U> relation) {
    this.relationsMap.put(t, relation);
  }

  public Map<T, Relation<U>> getRelationsMap() {
    return this.relationsMap;
  }

  public Set<T> getKeys() {
    return this.relationsMap.keySet();
  }

  public Set<U> getElements() {
    return this.relationsMap.keySet().stream().flatMap(event -> toStream(this.relationsMap.get(event)).flatMap(pair -> pair.stream())).collect(toSet());
  }

  public Set<U> getReachables(Set<T> ts, Set<U> initials) {
    Set<U> reachable = new HashSet<>(initials);
    while (true) {
      Set<U> nextReachable = new HashSet<>(reachable);
      for (T t : ts)
        nextReachable.addAll(this.getRelation(t).successors(reachable));
      if (reachable.equals(nextReachable))
        break;
      reachable = nextReachable;
    }
    return reachable;
  }

  public Set<U> getReachables(Set<U> initials) {
    return this.getReachables(this.relationsMap.keySet(), initials);
  }

  public Transitions<T, U> intersect(Set<U> us) {
    var intersected = new Transitions<T, U>();
    for (var t : this.getKeys())
      intersected.putRelation(t, this.getRelation(t).stream().filter(p -> us.contains(p.getFirst()) && us.contains(p.getSecond())).collect(toRelation()));
    return intersected;
  }

  public Transitions<T, U> reduct(Map<T, Relation<U>> tsMap) {
    var reductTransitionsMap = new HashMap<T, Relation<U>>();
    for (var t : tsMap.keySet()) {
      var mappedT = tsMap.get(t);
      assert mappedT != null;
      reductTransitionsMap.put(t, mappedT);
    }
    return new Transitions<T, U>(reductTransitionsMap);
  }

  public Transitions<T, U> restrict(Set<T> ts) {
    var restrictedRelationsMap = new HashMap<T, Relation<U>>();
    for (var t : ts) {
      var relation = this.relationsMap.get(t);
      assert relation != null;
      restrictedRelationsMap.put(t, relation);
    }
    return new Transitions<T, U>(restrictedRelationsMap);
  }

  public <V> Transitions<T, V> apply(Function<U, V> fun) {
    var transitionsMap = new HashMap<T, Relation<V>>();
    for (var t : this.relationsMap.keySet()) {
      var relation = this.relationsMap.get(t);
      if (relation == null)
        continue;
      transitionsMap.put(t, relation.apply(fun));
    }
    return new Transitions<T, V>(transitionsMap);
  }

  @Override
  public int hashCode() {
    return this.relationsMap.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    if (object == this)
      return true;
    try {
      Transitions<?, ?> other = (Transitions<?, ?>)object;
      return Objects.equals(this.relationsMap, other.relationsMap);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.relationsMap);
    return builder.toString();
  }
}
