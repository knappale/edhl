package edhl;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;
import util.Relation;


@NonNullByDefault
public abstract class Label {
  public static class Choice extends Label {
    private Label left, right;

    public Choice(Label left, Label right) {
      super();
      this.left = left;
      this.right = right;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Relation.union(this.left.interpretOver(system), this.right.interpretOver(system));
    }

    @Override
    public int getPrecedence() {
      return 12;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Choice other = (Choice)object;
      return Objects.equals(left, other.left)
          && Objects.equals(right, other.right);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, l -> l.hasPrecedence(this)));
      builder.append(" + ");
      builder.append(Formatter.parenthesised(this.right, l -> l.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Seq extends Label {
    private Label left, right;
 
    public Seq(Label left, Label right) {
      super();
      this.left = left;
      this.right = right;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Relation.seq(this.left.interpretOver(system), this.right.interpretOver(system));
    }

    @Override
    public int getPrecedence() {
      return 10;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.left, this.right);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Seq other = (Seq)object;
      return Objects.equals(left, other.left)
          && Objects.equals(right, other.right);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.left, l -> l.hasPrecedence(this)));
      builder.append("; ");
      builder.append(Formatter.parenthesised(this.right, l -> l.hasPrecedence(this)));
      return builder.toString();
    }
  }

  public static class Count extends Label {
    private int count;
    private Label label;

    public Count(int count, Label label) {
      this.count = count;
      this.label = label;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      Relation<Configuration> relation = Relation.reflexive(system.getConfigurations());
      for (int n = 0; n < this.count; n++)
        relation = Relation.seq(relation, this.label.interpretOver(system));
      return relation;
    }

    @Override
    public int getPrecedence() {
      return 2;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.count, this.label);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Count other = (Count)object;
      return Objects.equals(this.count, other.count)
          && Objects.equals(this.label, other.label);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.label, l -> l.hasPrecedence(this)));
      builder.append("^");
      builder.append(this.count);
      return builder.toString();
    }
  }

  public static class Iterate extends Label {
    private Label label;

    public Iterate(Label label) {
      super();
      this.label = label;
    }

    @Override
    public Relation<Configuration> interpretOver(TransitionSystem system) {
      return Relation.transitive(Relation.union(Relation.reflexive(system.getConfigurations()), this.label.interpretOver(system)));
    }

    @Override
    public int getPrecedence() {
      return 2;
    }

    @Override
    public int hashCode() {
      return Objects.hash(this.label);
    }

    @Override
    public boolean equals(@Nullable Object object) {
      if (this == object)
        return true;
      if (object == null)
        return false;
      if (getClass() != object.getClass())
        return false;
      Iterate other = (Iterate)object;
      return Objects.equals(this.label, other.label);
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(Formatter.parenthesised(this.label, l -> l.hasPrecedence(this)));
      builder.append("*");
      return builder.toString();
    }
  }

  public abstract int getPrecedence();

  public boolean hasPrecedence(Label other) {
    return this.getPrecedence() <= other.getPrecedence();
  }

  public abstract Relation<Configuration> interpretOver(TransitionSystem system);
}
