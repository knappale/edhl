package edhl;

import static java.util.stream.Collectors.toSet;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class Variable extends Formula {
  private String name;

  public Variable(String name) {
    this.name = name;
  }

  public String getName() {
    return this.name;
  }

  @Override
  public Set<Configuration> satisfactionSet(TransitionSystem system, Valuation valuation) {
    return system.getConfigurations().stream().filter(c -> c.getControlState().equals(valuation.getControlState(this))).collect(toSet());
  }

  @Override
  public int getPrecedence() {
    return 0;
  }

  @Override
  public int hashCode() {
    return this.name.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      Variable other = (Variable)object;
      return this.name.equals(other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.name;
  }
}
