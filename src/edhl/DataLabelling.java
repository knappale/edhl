package edhl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class DataLabelling {
  private Map<DataState, data.Valuation> labelling = new HashMap<>();

  public DataLabelling(Map<DataState, data.Valuation> dataLabelling) {
    this.labelling = dataLabelling;
  }

  public DataLabelling(Collection<DataConfiguration> dataConfigurations) {
    for (var dataConfiguration : dataConfigurations)
      this.labelling.put(dataConfiguration.getConfiguration().getDataState(), dataConfiguration.getDataLabel());
  }

  public data.@Nullable Valuation get(DataState dataState) {
    return labelling.get(dataState);
  }

  public Map<DataState, data.Valuation> getLabelling() {
    return this.labelling;
  }

  public DataLabelling reduct(Map<data.Attribute, data.Attribute> attributesMap) {
    var reductDataLabelling = new HashMap<DataState, data.Valuation>();
    for (var dataState : this.labelling.keySet()) {
      var dataLabel = this.labelling.get(dataState); assert dataLabel != null;
      reductDataLabelling.put(dataState, dataLabel.reduct(attributesMap));
    }
    return new DataLabelling(reductDataLabelling);
  }

  public DataLabelling restrict(Set<data.Attribute> attributes) {
    var restrictedDataLabelling = new HashMap<DataState, data.Valuation>();
    for (var dataState : this.labelling.keySet()) {
      var dataLabel = this.labelling.get(dataState); assert dataLabel != null;
      restrictedDataLabelling.put(dataState, dataLabel.restrict(attributes));
    }
    return new DataLabelling(restrictedDataLabelling);
  }

  @Override
  public int hashCode() {
    return this.labelling.hashCode();
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    if (object == this)
      return true;
    try {
      DataLabelling other = (DataLabelling)object;
      return Objects.equals(this.labelling, other.labelling);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.labelling);
    return builder.toString();
  }
}
