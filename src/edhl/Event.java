package edhl;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Relation;


@NonNullByDefault
public class Event extends CompositeEvent {
  private String name;

  public Event(String name) {
    this.name = name;
  }

  @Override
  public Relation<Configuration> interpretOver(TransitionSystem system) {
    return system.getRelation(this);
  }

  @Override
  public int getPrecedence() {
    return 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.name);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      Event other = (Event)object;
      return Objects.equals(this.name, other.name);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return this.name;
  }
}
