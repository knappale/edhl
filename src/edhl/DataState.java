package edhl;

public class DataState {
  private static int counter = 0;
  private int number;

  public DataState() {
    this.number = counter++;
  }

  @Override
  public String toString() {
    return "" + this.number;
  }
}
