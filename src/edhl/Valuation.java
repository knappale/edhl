package edhl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Formatter;


@NonNullByDefault
public class Valuation {
  private Map<Variable, ControlState> map;

  public Valuation() {
    this.map = new HashMap<>();
  }

  public Valuation(Map<Variable, ControlState> map) {
    this.map = new HashMap<>(map);
  }

  public @Nullable ControlState getControlState(Variable variable) {
    return this.map.get(variable);
  }

  public Valuation with(Variable variable, ControlState controlState) {
    Valuation v = new Valuation(this.map);
    v.map.put(variable, controlState);
    return v;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.map);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (object == null)
      return false;
    try {
      Valuation other = (Valuation)object;
      return Objects.equals(this.map, other.map);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    return Formatter.map(map);
  }
}
