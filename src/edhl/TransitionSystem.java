package edhl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Relation;
import util.Sets;

import static java.util.stream.Collectors.toSet;


@NonNullByDefault
public class TransitionSystem {
  private Set<Configuration> configurations;
  private Transitions<Event, Configuration> transitions;
  private Set<Configuration> initialConfigurations;
  private DataLabelling dataLabelling;

  private Set<Event> events;
  private Map<Set<Event>, Set<Configuration>> configurationsMap = new HashMap<>();
  private Map<Configuration, data.Valuation> labelling = new HashMap<>();

  public TransitionSystem(Transitions<Event, DataConfiguration> dataTransitions, Set<DataConfiguration> initialDataConfigurations) {
    this(dataTransitions.apply(DataConfiguration::getConfiguration),
         initialDataConfigurations.stream().map(DataConfiguration::getConfiguration).collect(toSet()),
         new DataLabelling(Sets.union(dataTransitions.getElements(), initialDataConfigurations)));
  }

  public TransitionSystem(Transitions<Event, Configuration> transitions, Set<Configuration> initialConfigurations, DataLabelling dataLabelling) {
    this.configurations = transitions.getReachables(initialConfigurations);
    this.transitions = transitions.intersect(this.configurations);
    this.initialConfigurations = initialConfigurations;
    this.dataLabelling = dataLabelling;

    this.events = this.transitions.getKeys();
    this.configurationsMap.put(this.events, this.configurations);
    for (var configuration : this.configurations) {
      data.Valuation dataLabel = this.dataLabelling.get(configuration.getDataState());
      assert dataLabel != null;
      this.labelling.put(configuration, dataLabel);
    }
  }

  public Set<Event> getEvents() {
    return this.events;
  }

  public Set<Configuration> getConfigurations() {
    return this.configurations;
  }

  public Set<ControlState> getControlStates() {
    return this.configurations.stream().map(Configuration::getControlState).collect(toSet());
  }

  public Set<Configuration> getConfigurations(Set<Event> events) {
    @Nullable Set<Configuration> configurations = this.configurationsMap.get(events);
    if (configurations == null) {
      configurations = this.transitions.getReachables(events, this.initialConfigurations);
      this.configurationsMap.put(events, configurations);
    }
    return configurations;
  }

  public Relation<Configuration> getRelation(Event event) {
    return this.transitions.getRelation(event);
  }

  public Relation<Configuration> getRelation(CompositeEvent compositeEvent) {
    return compositeEvent.interpretOver(this);
  }

  public Relation<Configuration> getRelation(Label label) {
    return label.interpretOver(this);
  }

  public data.Valuation getDataLabel(Configuration configuration) {
    var dataLabel = this.labelling.get(configuration);
    assert dataLabel != null;
    return dataLabel;
  }

  public boolean satisfies(Formula formula) {
    return formula.satisfactionSet(this).containsAll(this.initialConfigurations);
  }

  public Relation<Configuration> getTransitions(Label label) {
    return label.interpretOver(this);
  }

  public TransitionSystem reduct(Map<Event, CompositeEvent> eventsMap, Map<data.Attribute, data.Attribute> attributesMap) {
    var reductMap = new HashMap<Event, Relation<Configuration>>();
    for (var event : eventsMap.keySet()) {
      var mappedCompositeEvent = eventsMap.get(event);
      assert mappedCompositeEvent != null;
      var eventTransitions = this.getRelation(mappedCompositeEvent);
      reductMap.put(event, eventTransitions);
    }
    return new TransitionSystem(this.transitions.reduct(reductMap), this.initialConfigurations, this.dataLabelling.reduct(attributesMap));
  }

  public TransitionSystem restrict(Set<Event> events, Set<data.Attribute> attributes) {
    return new TransitionSystem(this.transitions.restrict(events), this.initialConfigurations, this.dataLabelling.restrict(attributes));
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.configurations,
                        this.transitions,
                        this.initialConfigurations,
                        this.dataLabelling);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      TransitionSystem other = (TransitionSystem)object;
      return Objects.equals(this.configurations, other.configurations)
          && Objects.equals(this.transitions, other.transitions)
          && Objects.equals(this.initialConfigurations, other.initialConfigurations)
          && Objects.equals(this.dataLabelling, other.dataLabelling);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("TransitionSystem[configurations=");
    builder.append(this.configurations);
    builder.append(", transitions=");
    builder.append(this.transitions);
    builder.append(", initialConfigurations=");
    builder.append(this.initialConfigurations);
    builder.append(", dataLabelling=");
    builder.append(this.dataLabelling);
    return builder.toString();
  }
}
