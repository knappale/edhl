package edhl;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;


@NonNullByDefault
public class Configuration {
  private ControlState controlState;
  private DataState dataState;

  public Configuration(ControlState controlState) {
    this.controlState = controlState;
    this.dataState = new DataState();
  }

  public ControlState getControlState() {
    return this.controlState;
  }

  public DataState getDataState() {
    return this.dataState;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.controlState,
                        this.dataState);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    try {
      Configuration other = (Configuration)object;
      return Objects.equals(this.controlState, other.controlState) &&
             Objects.equals(this.dataState, other.dataState);
    }
    catch (ClassCastException cce) {
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("Configuration [controlState=");
    builder.append(this.controlState);
    builder.append(", dataState=");
    builder.append(this.dataState);
    builder.append("]");
    return builder.toString();
  }
}
