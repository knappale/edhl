package edhl;

import java.util.Objects;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;

import util.Relation;

import static util.Relation.toRelation;


@NonNullByDefault
public class Action extends Label {
  private CompositeEvent compositeEvent;
  private data.Predicate dataPredicate;

  public Action(CompositeEvent compositeEvent) {
    this.compositeEvent = compositeEvent;
    this.dataPredicate = data.Predicate.True();
  }

  public Action(CompositeEvent compositeEvent, data.Predicate dataPredicate) {
    this.compositeEvent = compositeEvent;
    this.dataPredicate = dataPredicate;
  }

  @Override
  public Relation<Configuration> interpretOver(TransitionSystem system) {
    return system.getRelation(this.compositeEvent).stream().filter(p -> this.dataPredicate.holdsAt(data.Valuation.prePost(p.apply(c -> system.getDataLabel(c))))).collect(toRelation());
  }

  @Override
  public int getPrecedence() {
    return this.dataPredicate.equals(data.Predicate.True()) ? this.compositeEvent.getPrecedence() : 3;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.compositeEvent,
                        this.dataPredicate);
  }

  @Override
  public boolean equals(@Nullable Object object) {
    if (this == object)
      return true;
    if (object == null)
      return false;
    if (this.getClass() != object.getClass())
      return false;
    Action other = (Action)object;
    return Objects.equals(this.compositeEvent, other.compositeEvent)
        && Objects.equals(this.dataPredicate, other.dataPredicate);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.compositeEvent);
    if (!this.dataPredicate.equals(data.Predicate.True())) {
      builder.append(" // ");
      builder.append(this.dataPredicate);
    }
    return builder.toString();
  }
}
