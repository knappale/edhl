package edhl;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.jupiter.api.Test;

import data.IntegerValue;
import util.Pair;
import util.Relation;
import util.Sets;

class SatisfactionTest {
  @Test
  void satisfactionTest() {
    data.Attribute val = new data.Attribute("val", Sets.elements(new IntegerValue(0), new IntegerValue(1)));
    Event sw = new Event("switch");
    Variable x = new Variable("x");

    data.Valuation v0 = new data.Valuation().with(val, new IntegerValue(0));
    data.Valuation v1 = new data.Valuation().with(val, new IntegerValue(1));
    ControlState s0 = new ControlState("s0");
    ControlState s1 = new ControlState("s1");
    DataConfiguration c0 = new DataConfiguration(s0, v0);   
    DataConfiguration c1 = new DataConfiguration(s1, v1);   
    @NonNull Map<@NonNull Event, @NonNull Relation<@NonNull DataConfiguration>> map = new HashMap<>();
    map.put(sw, Relation.pairs(Sets.elements(new Pair<>(c0, c1), new Pair<>(c1, c0))));
    TransitionSystem system1 = new TransitionSystem(new Transitions<>(map), Sets.singleton(c1));
    TransitionSystem system2 = new TransitionSystem(new Transitions<>(map), Sets.singleton(c0));

    Formula f = new Formula.Bind(x, Formula.and(new Formula.Data(new data.Predicate.Eq(val, new IntegerValue(1))),
                                                new Formula.Diamond(new Action(sw, new data.Predicate.Eq(val.prime(), new IntegerValue(0))),
                                                                    new Formula.Diamond(new Action(sw, new data.Predicate.Eq(val.prime(), new IntegerValue(1))),
                                                                                        x))));
    // System.out.println(f);
    assertTrue(system1.satisfies(f));
    assertFalse(system2.satisfies(f));
}
}
