package edhl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import data.IntegerValue;
import util.Sets;

class FormulaTest {
  @Test
  void formulaTest() {
    data.Attribute val = new data.Attribute("val", Sets.elements(new IntegerValue(0), new IntegerValue(1)));
    Event sw = new Event("switch");
    Variable x = new Variable("x");

    Formula f = new Formula.Bind(x, Formula.and(new Formula.Data(new data.Predicate.Eq(val, new data.IntegerValue(1))),
                                                new Formula.Diamond(new Action(sw, new data.Predicate.Eq(val.prime(), new data.IntegerValue(0))),
                                                                    new Formula.Diamond(new Action(sw, new data.Predicate.Eq(val.prime(), new data.IntegerValue(1))),
                                                                                            x))));
    assertEquals("|x . val == 1 && <switch // val' == 0><switch // val' == 1>x", f.toString());
  }
}
