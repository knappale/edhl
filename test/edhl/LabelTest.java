package edhl;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import data.Predicate;

class LabelTest {
  @Test
  void labelTest() {
    Event e1 = new Event("e1");
    Event e2 = new Event("e2");
    Event e3 = new Event("e3");

    Label label = new Label.Iterate(new Label.Seq(new Label.Choice(new Action(e1), new Action(e2)),
                                                  new Action(e3, Predicate.False())));
    assertEquals("((e1 + e2); e3 // false)*", label.toString());
  }
}
