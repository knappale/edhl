package opspec;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;

import org.eclipse.jdt.annotation.NonNull;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import data.BooleanValue;
import data.Expression;
import data.IntegerValue;
import edhl.Action;
import edhl.CompositeEvent;
import edhl.ControlState;
import edhl.Event;
import edhl.Formula;
import edhl.Label;
import edhl.Variable;
import util.Sets;


class OperationalSpecificationTest {
  private static data.@NonNull Attribute chk = new data.Attribute("chk", Sets.elements(new BooleanValue(false), new BooleanValue(true)));
  private static data.@NonNull Attribute trls = new data.Attribute("trls", Sets.elements(new IntegerValue(0), new IntegerValue(1), new IntegerValue(2), new IntegerValue(3)));
  private static data.@NonNull Attribute cnt = new data.Attribute("cnt", Sets.elements(new IntegerValue(0), new IntegerValue(1), new IntegerValue(2), new IntegerValue(3)));

  private static @NonNull Event insertCard = new Event("insertCard");
  private static @NonNull Event enterPIN = new Event("enterPIN");
  private static @NonNull Event cancel = new Event("cancel");
  private static @NonNull Event ejectCard = new Event("ejectCard");
  private static @NonNull Event verifyPIN = new Event("verifyPIN");
  private static @NonNull Event correctPIN = new Event("correctPIN");
  private static @NonNull Event wrongPIN = new Event("wrongPIN");

  private static @NonNull ControlState Card = new ControlState("Card");
  private static @NonNull ControlState PIN = new ControlState("PIN");
  private static @NonNull ControlState Return = new ControlState("Return");
  private static @NonNull ControlState PINEntered = new ControlState("PINEntered");
  private static @NonNull ControlState Verifying = new ControlState("Verifying");
  private static @NonNull ControlState Idle = new ControlState("Idle");
  private static @NonNull ControlState Busy = new ControlState("Busy");

  private static @NonNull Variable x0 = new Variable("x0");
  private static @NonNull Formula f1 = new Formula.Bind(x0,
                                       new Formula.Jump(x0, Formula.and(new Formula.Diamond(new Action(insertCard, new data.Predicate.Eq(chk.prime(), false)), Formula.True()),
                                                            Formula.and(new Formula.Box(new Action(insertCard, new data.Predicate.Eq(chk.prime(), true)), Formula.False()),
                                                                        new Formula.Box(new Action(new CompositeEvent.Not(insertCard), data.Predicate.True()), Formula.False())))));
  private static @NonNull Formula f2 = new Formula.Box(new Action(new CompositeEvent.Seq(new CompositeEvent.Iterate(new CompositeEvent.All()), insertCard)),
                                                       Formula.and(new Formula.Diamond(new Action(enterPIN, new data.Predicate.Eq(chk.prime(), true)), Formula.True()),
                                                       Formula.and(new Formula.Diamond(new Action(enterPIN, new data.Predicate.Eq(chk.prime(), false)), Formula.True()),
                                                       Formula.and(new Formula.Diamond(new Action(cancel), Formula.True()),
                                                                   new Formula.Box(new Action(new CompositeEvent.Not(Sets.elements(enterPIN, cancel))), Formula.False())))));
  private static @NonNull Formula f3 = new Formula.Bind(x0, new Formula.Box(new Label.Seq(new Label.Iterate(new Action(new CompositeEvent.All())),
                                                                                          new Label.Choice(new Action(enterPIN, new data.Predicate.Eq(chk.prime(), true)),
                                                                                                           new Action(cancel))),
                                                                            Formula.and(new Formula.Diamond(new Action(ejectCard), x0),
                                                                            Formula.and(new Formula.Box(new Action(ejectCard), x0),
                                                                                        new Formula.Box(new Action(new CompositeEvent.Not(ejectCard)), Formula.False())))));
  private static @NonNull Formula f4 = new Formula.Bind(x0, new Formula.Box(new Label.Seq(new Label.Iterate(new Action(new CompositeEvent.All(), data.Predicate.True())),
                                                                                          new Label.Count(3, new Action(enterPIN, new data.Predicate.Eq(chk.prime(), false)))),
                                                                            x0));

  private static @NonNull OperationalSpecification ATM = new OperationalSpecification(Sets.empty(), Card, data.Predicate.True());

  @BeforeAll
  static void setupATM() {
    var Card2PIN = new TransitionsSpecification
        (Card,
         data.Predicate.True(),
         insertCard,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), 0)),
         PIN);
    var PIN2Card = new TransitionsSpecification
        (PIN,
         new data.Predicate.Eq(trls, 2),
         enterPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), new Expression.Add(trls, 1))),
         Card);
    var PIN2PIN = new TransitionsSpecification
        (PIN,
         new data.Predicate.Lt(trls, 2),
         enterPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), new Expression.Add(trls, 1))),
         PIN);
    var PIN2Return1 = new TransitionsSpecification
        (PIN,
         data.Predicate.True(),
         cancel,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), trls)),
         Return);
    var PIN2Return2 = new TransitionsSpecification
        (PIN,
         new data.Predicate.Leq(trls, 2),
         enterPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), true),
                            new data.Predicate.Eq(trls.prime(), new Expression.Add(trls, 1))),
         Return);
    var Return2Card = new TransitionsSpecification
        (Return,
         data.Predicate.True(),
         ejectCard,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), chk),
                            new data.Predicate.Eq(trls.prime(), trls)),
         Card);
    ATM = new OperationalSpecification(Sets.elements(Card2PIN, PIN2Card, PIN2PIN, PIN2Return1, PIN2Return2, Return2Card), Card, data.Predicate.True());
  }

  @Test
  void opSpecTest() {
    assertEquals(Sets.elements(cancel, ejectCard, enterPIN, insertCard), ATM.getEvents());
    assertEquals(Sets.elements(chk, trls), ATM.getAttributes());

    var ATMSys = ATM.getLargestSystem();
    assertEquals(Sets.elements(cancel, ejectCard, enterPIN, insertCard), ATMSys.getEvents());
    assertTrue(ATMSys.satisfies(ATM.getCharacteristicFormula()));

    assertTrue(ATMSys.satisfies(f1));
    assertTrue(ATMSys.satisfies(f2));
    assertTrue(ATMSys.satisfies(f3));
    assertTrue(ATMSys.satisfies(f4));
  }

  @Test
  void productTest() {
    var Card2PIN = new TransitionsSpecification
        (Card,
         data.Predicate.True(),
         insertCard,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), 0)),
         PIN);
    var PIN2Return = new TransitionsSpecification
        (PIN,
         data.Predicate.True(),
         cancel,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), trls)),
         Return);
    var Return2Card = new TransitionsSpecification
        (Return,
         data.Predicate.True(),
         ejectCard,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), chk),
                            new data.Predicate.Eq(trls.prime(), trls)),
         Card);
    var PIN2PINEntered = new TransitionsSpecification
        (PIN,
         new data.Predicate.Leq(trls, 2),
         enterPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), chk),
                            new data.Predicate.Eq(trls.prime(), trls)),
         PINEntered);
    var PINEntered2Verifying = new TransitionsSpecification
        (PINEntered,
         new data.Predicate.Leq(trls, 2),
         verifyPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), chk),
                            new data.Predicate.Eq(trls.prime(), trls)),
         Verifying);
    var Verifying2PIN = new TransitionsSpecification
        (Verifying,
         new data.Predicate.Lt(trls, 2),
         wrongPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), new Expression.Add(trls, 1))),
         PIN);
    var Verifying2Return = new TransitionsSpecification
        (Verifying,
         new data.Predicate.Leq(trls, 2),
         correctPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), true),
                            new data.Predicate.Eq(trls.prime(), new Expression.Add(trls, 1))),
         Return);
    var Verifying2Card = new TransitionsSpecification
        (Verifying,
         new data.Predicate.Eq(trls, 2),
         wrongPIN,
         data.Predicate.and(new data.Predicate.Eq(chk.prime(), false),
                            new data.Predicate.Eq(trls.prime(), new Expression.Add(trls, 1))),
         Card);

    var ATMp = new OperationalSpecification(Sets.elements(Card2PIN, PIN2Return, Return2Card, PIN2PINEntered, PINEntered2Verifying, Verifying2PIN, Verifying2Return, Verifying2Card), Card, data.Predicate.True());

    var Idle2Busy = new TransitionsSpecification
        (Idle,
         data.Predicate.True(),
         verifyPIN,
         new data.Predicate.Eq(cnt.prime(), cnt),
         Busy);
    var Busy2Idle1 = new TransitionsSpecification
        (Busy,
         data.Predicate.True(),
         correctPIN,
         new data.Predicate.Eq(cnt.prime(), new data.Expression.Mod(new data.Expression.Add(cnt, 1), 4)),
         Idle);
    var Busy2Idle2 = new TransitionsSpecification
        (Busy,
         data.Predicate.True(),
         wrongPIN,
         new data.Predicate.Eq(cnt.prime(), new data.Expression.Mod(new data.Expression.Add(cnt, 1), 4)),
         Idle);

    var CC = new OperationalSpecification(Sets.elements(Idle2Busy, Busy2Idle1, Busy2Idle2), Idle, new data.Predicate.Eq(cnt, 0));

    var ATMpCC = OperationalSpecification.product(ATMp, CC);
    // System.out.println(ATMpCC);

    var ATMpCCSys = ATMpCC.getLargestSystem();
    /*
    System.out.println(ATMpCCSys.getConfigurations().size());
    System.out.println("\n");
    for (var event : ATMpCCSys.getEvents()) {
      System.out.println(event);
      System.out.println(Formatter.separated(ATMpCCSys.getRelation(event).stream().collect(toSet()), "\n"));
    }
    */

    var eventsMap = new HashMap<@NonNull Event, @NonNull CompositeEvent>();
    eventsMap.put(insertCard, insertCard);
    eventsMap.put(enterPIN, new CompositeEvent.Seq(enterPIN, new CompositeEvent.Seq(verifyPIN, new CompositeEvent.Choice(correctPIN, wrongPIN))));
    eventsMap.put(cancel, cancel);
    eventsMap.put(ejectCard, ejectCard);
    var attributesMap = new HashMap<data.@NonNull Attribute, data.@NonNull Attribute>();
    attributesMap.put(chk, chk);
    attributesMap.put(trls, trls);
    var ATMpCCSysReduct = ATMpCCSys.reduct(eventsMap, attributesMap);
    /*
    System.out.println(ATMpCCSysReduct.getConfigurations().size());
    System.out.println(ATMpCCSysReduct.getEvents().size());
    System.out.println("\n");
    for (Event event : ATMpCCSysReduct.getEvents()) {
      System.out.println(event);
      System.out.println(Formatter.separated(ATMpCCSysReduct.getRelation(event).stream().collect(toSet()), "\n"));
    }
    */

    assertTrue(ATMpCCSysReduct.satisfies(ATM.getCharacteristicFormula()));
    // System.out.println("Finished checking characteristic formula.");

    // System.out.println(f1);
    assertTrue(ATMpCCSysReduct.satisfies(f1));
    // System.out.println(f2);
    assertTrue(ATMpCCSysReduct.satisfies(f2));
    // System.out.println(f3);
    assertTrue(ATMpCCSysReduct.satisfies(f3));
    // System.out.println(f4);
    assertTrue(ATMpCCSysReduct.satisfies(f4));
  }
}
