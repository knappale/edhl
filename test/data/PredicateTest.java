package data;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.junit.jupiter.api.Test;

import util.Sets;


@NonNullByDefault
class PredicateTest {
  @Test
  void statePredicateTest() {
    Attribute a = new Attribute("a", Sets.elements(new IntegerValue(0), new IntegerValue(1), new IntegerValue(2), new IntegerValue(3)));
    Valuation v = new Valuation().with(a, new IntegerValue(0));

    Predicate pred1 = Predicate.and(new Predicate.Neq(a, 1),
                                    new Predicate.Neq(a, 2));
    assertEquals("a != 1 && a != 2", pred1.toString());
    assertTrue(v.satisfies(pred1));

    Predicate pred2 = Predicate.and(new Predicate.Neq(a, 1),
                                    Predicate.or(new Predicate.Eq(a, 2),
                                                 new Predicate.Eq(a, 3)));
    assertEquals("a != 1 && (a == 2 || a == 3)", pred2.toString());
    assertFalse(v.satisfies(pred2));
  }

  @Test
  void transitionPredicateTest() {
    Attribute a = new Attribute("a", Sets.elements(new IntegerValue(0), new IntegerValue(1), new IntegerValue(2), new IntegerValue(3)));
    Valuation v = new Valuation().with(a, new IntegerValue(2)).with(a.prime(), new IntegerValue(3));

    Predicate pred1 = Predicate.and(new Predicate.Neq(a, 1),
                                    new Predicate.Neq(a, a.prime()));
    assertEquals("a != 1 && a != a'", pred1.toString());
    assertTrue(v.satisfies(pred1));

    Predicate pred2 = Predicate.or(new Predicate.Neq(a, 1),
                                   Predicate.and(new Predicate.Eq(a, a.prime()),
                                                 new Predicate.Eq(a, 3)));
    assertEquals("a != 1 || a == a' && a == 3", pred2.toString());
    assertTrue(v.satisfies(pred2));
  }

  @Test
  void valuationsTest() {
    Attribute a = new Attribute("a", Sets.elements(new IntegerValue(0), new IntegerValue(1), new IntegerValue(2), new IntegerValue(3)));
    Attribute b = new Attribute("b", Sets.elements(new BooleanValue(false), new BooleanValue(true)));
    Set<DecoratedAttribute> attributes = Sets.elements(a, b);

    assertEquals(Valuation.all(attributes).size(), 8);
    assertEquals(new Predicate.Eq(a, new IntegerValue(1)).getValuations(attributes), Sets.elements(new Valuation().with(a, new IntegerValue(1)).with(b, new BooleanValue(false)),
                                                                                                   new Valuation().with(a, new IntegerValue(1)).with(b, new BooleanValue(true))));
  }
}
